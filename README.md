# multi-crates

Test to use unpublished crates locally.

## ❓ How to use?

```sh
# Clone the project
$ git clone https://gitlab.com/jcs-workspace/rust/multi-crates

# Navigate to the `my_exe` directory
$ cd ./multi-crates/my_exe

# Build and run the project
$ cargo run
```

## 📁 Project Structure

```
my_lib  (the unpublished library/crate)
my_exe  (the executable project)
```

## 🤔 Explain

The project `my_lib` is the unpublished crate, and we can use it locally by
adding the following to your `Cargo.toml` file.

```toml
[dependencies]
my_lib = { path = "../my_lib" }
```

## 🔗 References

- [How to use a local unpublished crate?](https://stackoverflow.com/questions/33025887/how-to-use-a-local-unpublished-crate)
