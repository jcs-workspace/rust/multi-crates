use my_lib;

fn main() {
    println!("Hello, world!");
    let num = my_lib::add(1, 2);
    println!("{:?}", num);
}
